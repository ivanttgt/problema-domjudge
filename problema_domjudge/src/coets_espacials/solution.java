package coets_espacials;

import java.util.Scanner;

public class solution {
	// 3.14*(radi*radi)*altura
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			double r = sc.nextDouble();
			double h = sc.nextDouble();
			int fases = sc.nextInt();
			System.out.println(problema(r, h, fases));
		}
	}

	static Double problema(double r, double h, int fases) {
		Scanner sc = new Scanner(System.in);
		double combustible = (r*r)*h*3.141592;
		double fase_anterior = combustible;
		for (int j = 1; j < fases; j++) {
			fase_anterior = (fase_anterior/3)*2;
			combustible += fase_anterior;
		}
		return combustible;
	}
	
}
