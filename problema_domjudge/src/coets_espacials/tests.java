package coets_espacials;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class tests {
	@Test
	void test() {
		double r = 5;
		double h = 8;
		int fases = 2;
		double resposta = 1047.1973333333333;
		assertEquals(resposta,solution.problema(r,h,fases));
	}
	@Test
	void test2() {
		double r = 15;
		double h = 6;
		int fases = 7;
		double resposta = 11978.773940740743;
		assertEquals(resposta,solution.problema(r,h,fases));
	}
	
	@Test
	void test3() {
		double r = 15;
		double h = 65;
		int fases = 3;
		double resposta = 96996.653;
		assertEquals(resposta,solution.problema(r,h,fases));
	}
	
	@Test
	void test4() {
		double r = 33;
		double h = 1.5965;
		int fases = 4;
		double resposta = 13149.104518073334;
		assertEquals(resposta,solution.problema(r,h,fases));
	}
	
	@Test
	void test5() {
		double r = 15.18;
		double h = 99.173;
		int fases = 5;
		double resposta = 187018.34644733559;
		assertEquals(resposta,solution.problema(r,h,fases));
	}

}
